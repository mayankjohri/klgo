# Chapter Two - Basics of Language

Ok, I will be skipping all the history and boring stuff which most of us anyway skips.

## Installation

One can either download the binary or download the souce and compile it self.
This text will only discuss the installation of official installers.

### Windows
- Download the installer:
    Download the MSI installer for windows from [https://golang.org/dl/](https://golang.org/dl/). The latest version is at [https://storage.googleapis.com/golang/go1.7.1.windows-amd64.msi](https://storage.googleapis.com/golang/go1.7.1.windows-amd64.msi)
- Either Install using command line:
    msiexec /i go1.7.1.windows-amd64.msi /l*v goinst.log
- Or double click on the downloaded MSI file and follow the instructions present by the installer.


### MAC OSX
- Download the installer:
   Download the pkg installer for macOS from [https://golang.org/dl/](https://golang.org/dl/). The latest version is at [https://storage.googleapis.com/golang/go1.7.1.darwin-amd64.pkg](https://storage.googleapis.com/golang/go1.7.1.darwin-amd64.pkg)https://www.youtube.com/watch?v=fGChfVWMo5Mw
- double click on the downloaded file and follow normal process of installing any macOS application.


### *inx - Linux, BSD, Unix & other *inx
There are two ways to install GO.

* Method 1 (Official Method of Distributions)
  - Arch Linux
  - Debian
  - RedHat /CentOS


* Method 2
Download the zip file from the website and extract it on the folder.

## Configure

