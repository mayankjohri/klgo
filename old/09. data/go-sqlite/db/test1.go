package main

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	// var count int

	db, err := sql.Open("sqlite3", "test.db")
	if err != nil {
		fmt.Printf("open: %v\n", err)
		return
	}
	db.Exec("PRAGMA journal_mode=WAL;")

	db.Exec("CREATE TABLE test (id SERIAL, user TEXT NOT NULL, name TEXT NOT NULL);")
	db.Exec("INSERT INTO test (user, name) VALUES ('dajohi 2','David 2');")
	time.Sleep(30 * time.Second)
	// trans, err := db.Begin()
	// if err != nil {
	//  fmt.Printf("begin: %v\n", err)
	// }

	// s, err := trans.Prepare("INSERT INTO test (user, name) VALUES (?, ?);")
	// if err != nil {
	//  fmt.Printf("prepare: %v\n", err)
	// }

	// err = db.QueryRow("SELECT count(user) FROM test;").Scan(&count)
	// if err != nil {
	//  fmt.Printf("QueryRow: %v\n", err)
	// }

	// _, err = s.Exec("dajohi1", "David1")
	// if err != nil {
	//  fmt.Printf("exec: %v\n", err)
	// }
	// s.Close()
	db.Close()
}
