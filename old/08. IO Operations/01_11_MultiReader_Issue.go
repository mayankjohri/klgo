package main

import (
	"fmt"
	"io"
	"strings"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	} else if err == io.EOF {
		fmt.Println(err)
	}
}

func main() {
	r_header := strings.NewReader(`Hello Reader,
        `)
	r_body := strings.NewReader(`
How are you doing,
I had good time in Aligarh.`)
	r_tail := strings.NewReader(`
Bye,
Mayank J.
        `)

	// make a buffer to keep chunks that are read
	buf_size := []int{20, 10}
	for _, x := range buf_size {
		fmt.Println("Buffer size:", x)
		buf := make([]byte, x)

		mr := io.MultiReader(r_header, r_body, r_tail)

		for {
			count, err := mr.Read(buf)
			check(err)
			if count == 0 {
				break
			}
			fmt.Printf("%s", buf[:count])
		}
		fmt.Println()
	}
}
