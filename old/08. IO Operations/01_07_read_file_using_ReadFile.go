package main

import (
	"fmt"
	"io/ioutil"
)

func check(err error) {
	if err != nil {
		fmt.Print(err)
		panic(0)
	}
}

func line() {

	fmt.Println("------------------")
}

func main() {
	b, err := ioutil.ReadFile("prayers.txt")
	check(err)
	fmt.Println("Printing as bytes")
	line()
	fmt.Println(b)   // print as 'bytes'
	str := string(b) // convert to a 'string'
	fmt.Println("Printing as string")
	line()
	fmt.Println(str) // print the 'string'
}
