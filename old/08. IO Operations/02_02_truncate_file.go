package main

import (
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func main() {
	name := "testfile_truncate.txt"
	fi, err := os.OpenFile(name, os.O_RDWR|os.O_CREATE, 0764)
	check(err)
	defer func() {
		err := fi.Close()
		check(err)
	}()
	fi.WriteString(`Om Bhur bhuvah swah, tatsavitur
varenyam bhargo devasya dhimahi
Dhiyo yo nah prachodayat`)

	fi.Truncate(0)
	fi.Seek(0, 0)
	fi.WriteString(`O God, The Giver of life. Remover of pains and
sorrows Bestower of happiness, and Creator of the Universe.
Thouart most luminous, pure and adorable. We meditate on
Thee May Thou inspire and guide our intellect in the
right direction.`)
}
