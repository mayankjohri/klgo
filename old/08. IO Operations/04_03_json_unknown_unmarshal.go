package main

import (
	"encoding/json"
	"fmt"
)

type Questions struct {
	ID       float64
	Question string
	Live     bool
}

func main() {
	var questions [2]Questions
	questions[0].ID = 0.0
	questions[0].Question = "How do I list all files of a directory?"
	questions[0].Live = true
	questions[1].Question = "How do I parse values from a JSON file?"
	questions[1].Live = false
	questions[1].ID = 0.1

	quests, _ := json.Marshal(questions)
	fmt.Println(string(quests))
	fmt.Println("*****")
	json.Unmarshal(quests, questions)
	fmt.Println(questions)
}
