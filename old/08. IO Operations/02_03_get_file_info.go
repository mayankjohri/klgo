package main

import (
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func main() {
	name := "testfile_truncate.txt"

	fileInfo, err := os.Stat(name)
	check(err)

	fmt.Println("Is Directory: ", fileInfo.IsDir())
	fmt.Println("File Name:", fileInfo.Name())
	fmt.Println("File Size in bytes:", fileInfo.Size())
	fmt.Println("File Permissions:", fileInfo.Mode())
	fmt.Println("File Last modified:", fileInfo.ModTime())
}
