package main

import "fmt"

// C language syntax
// int sum (int x, int y)

// Go Language Syntax
func sum(x int, y int) int {
	return x + y
}

// This will not run.
func printHello() {
	fmt.Println("Hello !")
}

func main() {
	fmt.Println(sum(10, 122))
}
