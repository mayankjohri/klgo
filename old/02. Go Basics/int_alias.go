package main

import "fmt"

func main() {
	var (
		data       byte
		extra_data rune
	)

	fmt.Println(data, extra_data)
}
