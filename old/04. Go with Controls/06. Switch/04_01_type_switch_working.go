package main

import "fmt"

func act_type(x interface{}) {
    switch x.(type) {
    case bool:
        fmt.Println("boolean")
    case int, int8, int16, int32, int64:
        fmt.Println("int")
    case string:
        fmt.Println("String")
    default:
        fmt.Println("Sorry not processing this data type")
    }
}

func main() {
    var x = 10
    act_type(x)
    act_type("Mayank")
    act_type(22.00)
}
