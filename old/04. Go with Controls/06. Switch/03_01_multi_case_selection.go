package main

import "fmt"

func main() {
    var x string
    fmt.Println("... Periodic table ...")
    fmt.Print("Enter Symbol: ")
    
    fmt.Scanf("%s", &x)
    switch x {
    case "h", "he":
        fmt.Println("Period 1")
    case "li", "be", "b", "c", "n", "o", "f", "ne":
        fmt.Println("Period 2")
    case "na", "mg", "al", "si", "p", "s", "cl", "ar":
        fmt.Println("Period 3")
    case "k", "ca", "sc", "ti", "v", "cr", "mn", "fe", "co",
        "ni", "cu", "zn", "ga", "ge", "as", "se", "br", "kr":
        fmt.Println("Period 4")
    }
}
