package main

import "fmt"

func main() {
    a := 100
    {
        fmt.Println(a)
        c, b := 0, 0
        fmt.Println(a, b, c)
    }
    fmt.Println(a)
}
