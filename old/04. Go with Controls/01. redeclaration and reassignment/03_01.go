package main

import "fmt"

func main() {
    i := "K.V. Pauly" //scope: main
    j := 11
    for i := 'M'; i < 'N'; i++ {
        // i shadowed inside this block
        fmt.Println("inside for:", i, j)
    }
    fmt.Println(i, j)

    if i := "Āryāvarta"; len(i) == j {
        // i shadowed inside this block
        fmt.Println("inside if:", i, j)
    } else {
        // i shadowed inside this block
        fmt.Println("inside else:", i, len(i), j)
    }
    fmt.Println(i, j)
}
