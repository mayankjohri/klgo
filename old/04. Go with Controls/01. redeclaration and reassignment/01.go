package main

import "fmt"

func main() {
    a := 100
    {
        fmt.Println(a)
        // new local variable `a` 
        a, b := 0, 0
        fmt.Println(a, b)
    }
    fmt.Println(a)
}