package main

import (
  "fmt"
  "strings"
)


type User struct {
  Name string
  Id int
  firstName string
  lastName string
}

func NewUser(userName string, id int) *User{
  
  name := strings.Fields(userName)
  return &User{userName, id, name[0], name[1]}
}

func main() {
  mayank := NewUser("Mayank Johri", 10001)

	fmt.Println(mayank)
}
