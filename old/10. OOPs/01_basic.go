package main

import "fmt"

type Candidates struct {
    name string
    uniqueId int
    Topic string
}
 
func (c Candidates) Details() {
    fmt.Println(c.name, "from", c.Topic, "has UID", c.uniqueId)
}

func main() {
  rakesh := Candidates{"Rakesh", 100101, "Civil"}
  rakesh.Details()
}
