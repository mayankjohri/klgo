package main

import "fmt"

func power(exponent int) func(int) int {
	pow := func(base int) int {
		power := 1
		for i := 1; i <= exponent; i++ {
			power = power * base
		}

		return power
	}
	return pow
}

func main() {

  // square is the closure
	square := power(2)

	fmt.Println(square(14))
	
  cube := power(3)

	fmt.Println(square(4))
  fmt.Println(cube(4))

}
