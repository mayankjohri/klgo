package main

import "fmt"

func AddAll(vals ...int) int{
  a, b, c, d := 12, 23, 1, 4 // different defaults values 
  vals_len := len(vals)
  switch {
  case vals_len == 1:
    a = vals[0]
  case vals_len == 2:
    a = vals[0]
    b = vals[1]
  case vals_len == 3:
    a = vals[0]
    b = vals[1]
    c = vals[2]
  case vals_len >= 4:
    a = vals[0]
    b = vals[1]
    c = vals[2]
    d = vals[3]
  }
  return a + b + c + d
}

func main() {
	fmt.Println(AddAll(1, 2, 4, 2, 23, 23423, 234234))
	fmt.Println(AddAll(1, 2, 4, 2))
	fmt.Println(AddAll(1 ))
	fmt.Println(AddAll(4, 2))
}
