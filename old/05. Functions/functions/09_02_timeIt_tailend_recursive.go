package main

import (
	"log"
	"strconv"
	"time"
)

func timeIt(func_name string, startTime time.Time) {
	timeTaken := time.Since(startTime).Nanoseconds()
	log.Printf("%s took time to execute: %d", func_name, timeTaken)
}

func seq(x int, total int) int {
	if x != 0 {
		return seq(x-1, x+total)
	} else {
		return total
	}
}

func seq_test(x int) {
	defer timeIt("for "+strconv.Itoa(x), time.Now())
	seq(x, 1)
}

func main() {
	for x := 1; x < 100000; x = x + 1000 {
		seq_test(x)
	}
}
