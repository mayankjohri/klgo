package main

import "fmt"

func main() {
	A := 10 // 0b1010
	B := 3  // 0b011

	fmt.Println("A&B:", A&B)   // 0b1010 & 0b0011 => 0b10 => 2
	fmt.Println("A|B:", A|B)   // 0b1010 | 0b0011 => 0b1011 => 11
	fmt.Println("A^B:", A^B)   // 0b1010 ^ 0b0011 => 0b1001 => 9
	fmt.Println("A<<2:", A<<2) // 0b1010 << 2  => 0b101000 => 40
	fmt.Println("A>>2:", A>>2) // 0b1010 >> 2 => 0b10 => 2
}
