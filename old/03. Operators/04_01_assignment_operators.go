package main

import "fmt"

func main() {
	var a, b, c uint32
	a = 111
	b = 33

	c = a + b
	fmt.Println("c = a + b : ", c)
	c += a + b
	fmt.Println("c += a + b : ", c)
	c -= a + b
	fmt.Println("c -= a + b : ", c)
	c *= a + b
	fmt.Println("c *= a + b : ", c)
	c /= a + b
	fmt.Println("c /= a + b : ", c)
	c %= a + b
	fmt.Println("c %= a + b : ", c)
	c <<= a + b
	fmt.Println("c <<= a + b : ", c)
	c >>= a + b
	fmt.Println("c >>= a + b : ", c)
	c &= a + b
	fmt.Println("c &= a + b : ", c)
	c ^= a + b
	fmt.Println("c ^= a + b : ", c)
	c |= a + b
	fmt.Println("c |= a + b : ", c)
}
