package main

import "fmt"

func main() {

	nums := []int{
		36, 31, 64,
		45, 2, 1, 12, 6, 7, 38, 18,
		6, 7, 22, 21,
	}
	fmt.Println("Original array:", nums)
	slice_1 := nums[2:6]
	slice_2 := make([]int, len(slice_1))

	copy(slice_2, slice_1)
	slice_2 = append(slice_2, 10, 20, 30)
	fmt.Println("slice_1", slice_1)
	fmt.Println("slice_2", slice_2)
	fmt.Println("Original array:", nums)
}
