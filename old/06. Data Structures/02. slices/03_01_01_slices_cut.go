package main

import (
	"fmt"
)

func main() {

	nums := []int{
		36, 31, 64,
		45, 2, 1, 12, 6, 7, 38, 18,
		6, 7, 22, 21,
	}

	fmt.Println("Original array:", nums)
	fmt.Println("Original slice 1:", nums[:2])
	fmt.Println("Original slice 1:", nums[6:])
	slice_1 := append(nums[:2], nums[6:]...)
	fmt.Println("slice_1", slice_1)
	fmt.Println("Original array:", nums)
}
