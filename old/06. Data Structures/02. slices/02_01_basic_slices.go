package main

import "fmt"

func main() {

	nums := []int{
		36, 31, 64,
		45, 2, 1, 12, 6, 7, 38, 18,
		6, 7, 38, 18,
	}
	new_nums := nums[2:6]
	new_nums = append(new_nums, 22)
	fmt.Println("Original array is:", nums)
	fmt.Println("Slice is:", new_nums)

	r_nums := nums[2:6:7]
	r_nums = append(r_nums, 134)
	fmt.Println("Original array is:", nums)
	fmt.Println(r_nums)

	r1_nums := nums[2:6:6]
	r1_nums = append(r1_nums, 34)
	fmt.Println("Original array is:", nums)
	fmt.Println(r1_nums)
}
