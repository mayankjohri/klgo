package main

import (
	"fmt"
)

func main() {

	nums := []int{
		36, 31, 64, 45, 2,
		1, 12, 6, 7, 38,
		18, 6, 7, 22,
	}

	fmt.Println("Original array:", nums)
	var i = 3
	nums = append(nums[:i], nums[i+3:]...)
	fmt.Println("New array:", nums)
}
