package main

import "fmt"

func main() {

	nums := []int{
		36, 31, 64,
		45, 2, 1, 12, 6, 7, 38, 18,
		6, 7, 22, 21,
	}
	fmt.Println("Origial array is:", nums, len(nums), cap(nums))
	fmt.Println("Slice is nums[:]:", nums[:], len(nums[:]), cap(nums[:]))
	fmt.Println("Slice is nums[:6]:", nums[:6], len(nums[:6]), cap(nums[:6]))
	fmt.Println("Slice is nums[:1]:", nums[:1], len(nums[:1]), cap(nums[:1]))

	fmt.Println("Slice is nums[1:6]:", nums[1:6], len(nums[1:6]), cap(nums[1:6]))

	fmt.Println("Slice is nums[2:6]:", nums[2:6], len(nums[2:6]), cap(nums[2:6]))
	fmt.Println("Slice is nums[2:]:", nums[2:], len(nums[2:]), cap(nums[2:]))
	fmt.Println("Slice is nums[5:5:7]:", nums[5:5:7], len(nums[5:5:7]), cap(nums[5:5:7]))
	fmt.Println("Slice is nums[2:2]:", nums[2:2], len(nums[2:2]), cap(nums[2:2]))

	// the below code will fail, if len of nums is
	// less than 20.
	// fmt.Println("Slice is nums[:]:", nums[20:])
}
