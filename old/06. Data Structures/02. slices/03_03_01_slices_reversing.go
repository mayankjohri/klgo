package main

import "fmt"

func main() {
	nums := []int{
		36, 31, 64, 45, 2,
		1, 12, 6, 7, 38,
		18, 6, 7, 22,
	}

	fmt.Println("Original array:", nums)
	for i := len(nums)/2 - 1; i >= 0; i-- {
		opp := len(nums) - 1 - i
		nums[i], nums[opp] = nums[opp], nums[i]
	}
	fmt.Println("Updated array:", nums)
}
