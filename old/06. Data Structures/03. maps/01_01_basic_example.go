package main

import (
	"fmt"
)

func main() {
	var pincode_locality = make(map[int]string)

	pincode_locality[462003] = "SOUTH T.T. NAGAR B.O."
	pincode_locality[465680] = "Naahli B.O"
	pincode_locality[465680] = "Padlyadan B.O"
	pincode_locality[465680] = "Seenkaturkipura B.O"
	pincode_locality[465691] = "Dhatarawda B.O"
	pincode_locality[465691] = "Gagorani B.O"
	pincode_locality[465691] = "Jhaadmau B.O"
	pincode_locality[465697] = "Bhainswamataji B.O"
	pincode_locality[465697] = "Biaora Mandu B.O"
	pincode_locality[466651] = "Muktarnagar B.O"

	fmt.Println(pincode_locality)
}
