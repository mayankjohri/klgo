package main

import "fmt"

type User struct {
	Name string
	Id   int
}

func main() {
	mayank := new(User)
	fmt.Println(mayank)
	mayank.Name = "Mayank Johri"
	fmt.Println(mayank)
}
