package main

import "fmt"

type Users struct {
	UserName string
	UserId   int32
}

func main() {
	roshan := Users{"Roshan Musheer", 10001}
	ramesh := roshan
	fmt.Println(ramesh, roshan)
	ramesh.UserName = "Ramesh"
	fmt.Println(ramesh, roshan)
}
