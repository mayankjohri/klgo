package main

import (
	"fmt"
	"strings"
)

// Parent
type Users struct {
	Name string
	Id   int32
}

func (u Users) SetName(name string) {
	fmt.Println("In Users SetName")
	u.Name = strings.TrimSpace(name)
}

// Child
type Managers struct {
	Users

	Reportee bool
}

func (u Managers) SetName(name string) {
	fmt.Println("In Managers SetName")
	u.Name = strings.TrimSpace(name)
}

func main() {
	// mayank := Managers{"User": {"Name": "Mayank", "Id": 1001},
	// 	"Reportee": true,
	// }
	var mayank Managers
	mayank.Id = 10001
	mayank.Name = "Testing"
	mayank.Reportee = true
	fmt.Println(mayank)
	mayank.SetName("MJ")
	fmt.Println(mayank)
}
