package main

import "fmt"

// Starting Candidates Struct
type Candidates struct {
	name     string
	uniqueId int
	Topic    string
}

func (c Candidates) Details() {
	fmt.Println(c.name, "from", c.Topic, "has UID", c.uniqueId)
}

// Ending Candidates Struct

func main() {
	rakesh := Candidates{"Rakesh", 100101, "Civil"}
	rakesh.Details()
	// Just for fun
	Candidates{"MJ", 10000}.Details()
}
