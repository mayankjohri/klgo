package main

import "fmt"

func main() {
	A := "Mayank Johri"
	B := "mayank johri"

	fmt.Println(A, B)
	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
	fmt.Println("~~~~~~~~~~~~~~~~~")
	A = "Mayank Johri"
	B = "Mayank johri"

	fmt.Println(A, B)
	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
	fmt.Println("~~~~~~~~~~~~~~~~~")
	A = "Mayank Johri"
	B = "Mayank Johri"
	fmt.Println(A, B)
	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
	fmt.Println("~~~~~~~~~~~~~~~~~")
	A = "Mayank Johri"
	B = "Mayank Johri ."

	fmt.Println(A, B)
	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
	fmt.Println("~~~~~~~~~~~~~~~~~")
	A = "mayank Johri"
	B = "Mayank Johri ."

	fmt.Println(A, B)
	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
	fmt.Println("~~~~~~~~~~~~~~~~~")
}
