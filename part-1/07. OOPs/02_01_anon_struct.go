package main

import "fmt"

func main() {
	type pointinspaceandtime struct {
		x, y, z int64
		t       float64
	}

	p := pointinspaceandtime{
		2, 10, 101, 22.113,
	}

	fmt.Println(p.x, p.y, p.z, p.t)
}
