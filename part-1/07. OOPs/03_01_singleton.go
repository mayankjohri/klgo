package main

import "fmt"

type Circle struct {
	Radius float64
	Area   float64
}

var PI = 3.14
var circle *Circle

func NewCircle(radius float64) *Circle {

	area := PI * radius * radius
	if circle == nil {
		circle = &Circle{radius, area}
	} else {
		circle.Radius = radius
		circle.Area = area
	}
	return circle
}

func main() {
	a := NewCircle(10)
	fmt.Println(a)

	b := NewCircle(100)
	fmt.Println(a, b)
}
