package main

import (
	"fmt"
	"math"
)

const (
	PI float64 = 3.141592653589
)

type Circle struct {
	area   float64
	radius float64
}

// Getter Function
func (c Circle) Radius() float64 {
	return c.radius
}

// Setter Function
func (c *Circle) SetRadius(val float64) {
	c.radius = val
	c.area = PI * val * val
}

// Getter Function
func (c Circle) Area() float64 {
	return c.area
}

// Setter Function
func (c *Circle) SetArea(val float64) {
	c.area = val
	c.radius = math.Sqrt(val / PI)
}
func main() {
	// c := Circle{area: 10, radius: 12}
	c := Circle{radius: 12}
	fmt.Println(c.radius, c.area)
	fmt.Println(c.Radius(), c.Area())
	// Area will not update.
	c.radius = 20
	fmt.Println(c.Radius(), c.Area())
	c.SetRadius(10)
	fmt.Println(c.Radius(), c.Area())
}
