package main

import (
	"fmt"
)

type SmartString string

func (s SmartString) smallest() string {
	small := rune(s[0])
	for _, c := range s[1:] {
		if c < small {
			small = c
		}
	}
	return string(small)
}

func main() {
	var ss SmartString
	ss = "WElcomeToAryavarta"
	fmt.Println(ss.smallest())
}
