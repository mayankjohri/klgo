// go run inheritance.go
package main
import "fmt"

type Fruit struct{
    name string
    seeds int
}

func (f *Fruit) countForSeeds(){
    fmt.Printf("%s have %d seeds\n", f.name, f.seeds)
}

func (f *Fruit) Name() string{
  return f.name
}

func (f *Fruit) SetName(name string){
  f.name = name
}

type Apple struct{
    Fruit
}

func main(){
    //var f = Fruit{"Fruit", 2}
    var f = new(Fruit) //{"Fruit", 2}
    f.SetName("Mango")
  
    f.countForSeeds()
    var apple = Apple{Fruit{"Apple", 1}}
    apple.countForSeeds()
}
