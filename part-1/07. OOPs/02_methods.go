package main

import (
  "fmt"
  "math"
)

const (
  PI float64 = 3.141592653589
)

type Circle struct {
  radius float64 
  area float64 
}

func (c Circle) Radius()  float64{
  return c.radius
}

func (c *Circle) SetRadius(val float64) {
  c.radius = val
  c.area = PI * val * val
  fmt.Println(c)
}

func (c Circle) Area() float64{
  return c.area
}

func (c *Circle) SetArea(val float64) {
  c.area = val
  c.radius = math.Sqrt(val/PI)
}

func main() {
  c := new(Circle)
  c.SetRadius(10)
	fmt.Println(c.Radius(), c.Area())
}
