package main

import "fmt"

func AssertType(i interface{}, Type string) bool {
	return fmt.Sprintf("%T", i) == Type
}

func main() {
	var i interface{} = 11
	var msg interface{} = "Welcome"
	fmt.Println(AssertType(i, "int"))
	fmt.Println(AssertType(msg, "int"))
	fmt.Println(AssertType(i, "string"))
	fmt.Println(AssertType(msg, "string"))
}
