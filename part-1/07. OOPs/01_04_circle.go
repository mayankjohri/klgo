package main

import (
	"fmt"
	"math"
)

const (
	PI float64 = 3.141592653589
)

type Circle struct {
	radius float64
}

// Getter Function
func (c Circle) Area() float64 {
	return PI * c.radius * c.radius
}

// Setter Function
func (c *Circle) SetArea(val float64) {
	c.radius = math.Sqrt(val / PI)
}

func main() {
	c := new(Circle)
	fmt.Println(c.radius, c.Area())
	// Area will not update.
	c.radius = 20
	fmt.Println(c.radius, c.Area())
	c.radius = 10
	fmt.Println(c.radius, c.Area())
}
