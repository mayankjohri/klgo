package main

import "fmt"

type Interface interface {
	Method()
}

type Type struct {
	S string
}

func (t *Type) Method() {
	if t == nil {
		fmt.Println("<nil>")
		return // avoiding else
	}
	fmt.Println(t.S)
}

func details(i Interface) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var i Interface

	var t *Type
	i = t
	details(i)
	i.Method()

	i = &Type{"Geh! Geh! Geh!"}
	details(i)
	i.Method()
}
