// gotcha_00.go
package main

import "fmt"

type User interface {
	GetName() string
}

type Admin struct {
	name string
}

func (u *Admin) GetName() string {
	return u.name
}

/*
func (u *Admin) SetName(name string) {
	u.name = name
}
*/
/*
func (u User) GetName() string {
	return u.name
}
*/
func Welcome(u User) {
	fmt.Println("Welcome to", u.GetName())
}

func main() {
	var mj User
	mj = &Admin{"MJ"}
	Welcome(mj)
	/*
		mj.SetName("Mayank")
		Welcome(mj)
	*/
}
