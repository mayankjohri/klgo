package main

import "fmt"

func AssertIntType(i interface{}, Type string) bool {
	return fmt.Sprintf("%T", i) == Type
}

func main() {
	var i interface{} = 11
	var msg interface{} = "Welcome"
	fmt.Println(AssertIntType(i, "int"))
	fmt.Println(AssertIntType(msg, "int"))
	fmt.Println(AssertIntType(i, "string"))
	fmt.Println(AssertIntType(msg, "string"))
}
