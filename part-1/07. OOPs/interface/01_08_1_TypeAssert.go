package main

import "fmt"

func AssertIntType(i interface{}) bool {
	if _, err := i.(int); !err {
		return false
	}
	return true
}

func main() {
	fmt.Println(AssertIntType(12))
	fmt.Println(AssertIntType("12"))
}
