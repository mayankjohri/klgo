package main

import "fmt"

type Class struct {
	id         int16
	class_name string
	section    string
}

type Student struct {
	id int64
	*Class
	first_name, last_name string
}

func main() {
	s := Student{
		10101,
		&Class{
			101,
			"XII",
			"PCM",
		},
		"Sachin",
		"Shah",
	}

	fmt.Println("student:", s)
	fmt.Println(s.first_name, s.last_name, "is in class", s.class_name, s.section)
}
