package main

import (
	"errors"
	"fmt"
)

// SumIt Code comments
func SumIt(x int, y int) (int, error) {
	var err error
	var sum int
	if x > 200 {
		return sum, errors.New("x is more than 200")
	}
	return x + y, err
}

// SumIt1 Code is as follows
func SumIt1(x int, y int) (int, error) {
	var err error
	var sum int
	if x > 200 {
		return sum, fmt.Errorf("sorry, value of x is more than 200")
	}
	return x + y, err

}
func main() {
	val, err := SumIt(10, 22)
	fmt.Println(val, err)
	val, err = SumIt(1011, 22)
	fmt.Println(val, err)
	val, err = SumIt1(1011, 22)
	fmt.Println(val, err)
}
