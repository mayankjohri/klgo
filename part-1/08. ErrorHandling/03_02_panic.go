package main

import "fmt"

func TestPanic() error {
	panic("This is Panic")
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from panic:", r)
		}
	}()
	TestPanic()
}
