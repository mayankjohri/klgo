package main

import (
	"fmt"
	"log"
	"os"
)

// Creating different logs for different use case.
var (
	WarningLogger func(string)
	InfoLogger    func(string)
	ErrorLogger   func(string)
)

func init() {

	file, err := os.OpenFile("logsFile.log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	/*
		infoL := log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
		warningL := log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
		errorL := log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	*/
	logme := func(logType string) func(string) {
		logF := func(data string) {
			logT := log.New(file, logType+": ", log.Ldate|log.Ltime|log.Lshortfile)
			logT.Output(2, data)
			fmt.Println(logType+":", data)
		}
		return logF
	}
	InfoLogger = logme("Info")
	WarningLogger = logme("Warning")
	ErrorLogger = logme("Error")
}

func main() {
	InfoLogger("Lets Start the application...")
	InfoLogger("Processing the database")

	WarningLogger("Warning Waring, App might fail as db is missing")
	InfoLogger("Trying to create new db file")
	ErrorLogger("Opps, Error encounter, failed to create new db")

}
