package main

import (
	"bytes"
	"fmt"
	"log"
)

func main() {
	var (
		buf    bytes.Buffer
		logger = log.New(&buf, "logger: ", log.Lshortfile)
	)
	logger.Print("This is logging")
	logger.Println("I am continuing the logging.")

	fmt.Print(&buf)
}
