package main

import (
	"log"
	"os"
)

// Creating different logs for different use case.
var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
)

func init() {
	file, err := os.OpenFile("logsFile.txt",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	InfoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func main() {
	InfoLogger.Print("Lets Start the application...")
	InfoLogger.Print("Processing the database")
	WarningLogger.Print("Warning Waring, App might fail as db is missing")
	InfoLogger.Print("Trying to create new db file")
	ErrorLogger.Print("Opps, Error encounter, failed to create new db")
}
