package main

import (
	"log"
	"os"
)

func SetLogFile(fileName string) {
}

func main() {
	// If the file doesn't exist, create it or append to the file
	file, err := os.OpenFile("logFile.txt",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	log.SetOutput(file)

	log.Println("This is a log")
}
