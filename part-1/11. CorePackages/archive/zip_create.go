package main

import (
	"archive/zip"
	"fmt"
	"log"
	"os"
)

func main() {

	// Step 1: Create a Zip file
	fmt.Println("Step 1: creates a Zip File")
	target := "dummy.zip"
	zipFile, err := os.Create(target)
	if err != nil {
		log.Fatal(err)
	}
	defer zipFile.Close()
	// step 2: Create tar writer
	fmt.Println("Step 2: Create tar writer")
	w := zip.NewWriter(zipFile)

	// step 3: generate data to tar
	fmt.Println("Step 3: generate data to tar")
	var files = []struct {
		Name, FileData string
	}{
		{"readme.txt", "This is sample readme file"},
		{"data.txt", "Dummy Data file\nThis is dummy data."},
	}
	fmt.Println("Adding files to zip archive")
	for _, file := range files {
		fmt.Println("  Adding file:", file.Name)
		f, err := w.Create(file.Name)
		if err != nil {
			log.Fatal(err)
		}
		_, err = f.Write([]byte(file.FileData))
		if err != nil {
			log.Fatal(err)
		}
	}

	// Make sure to check the error on Close.
	err = w.Close()
	if err != nil {
		log.Fatal(err)
	}
}
