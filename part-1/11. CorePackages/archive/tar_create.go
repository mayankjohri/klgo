package main

import (
	"archive/tar"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	// Step 1: Create a buffer
	var buf bytes.Buffer
	// step 2: Create tar writer
	tw := tar.NewWriter(&buf)
	// step 3: generate data to tar
	var files = []struct {
		Name, FileData string
	}{
		{"readme.txt", "This is sample readme file"},
		{"data.txt", "Dummy Data file\nThis is dummy data."},
	}
	// step 4: adding file details to tar header
	for _, file := range files {
		hdr := &tar.Header{
			Name: file.Name,
			Mode: 0600,
			Size: int64(len(file.FileData)),
		}
		if err := tw.WriteHeader(hdr); err != nil {
			log.Fatal(err)
		}
		// Step 5: //writing file data in tar
		if _, err := tw.Write([]byte(file.FileData)); err != nil {
			log.Fatal(err)
		}
	}
	if err := tw.Close(); err != nil {
		log.Fatal(err)
	}

	// Open and iterate through the files in the archive.
	// Step 1: Create Tar Reader
	tr := tar.NewReader(&buf)
	// loop to read all files in tar `tr`
	var hdr *tar.Header
	var err error
	for {
		// Step 2: getting file handle
		if hdr, err = tr.Next(); err == io.EOF {
			break // End of archive
		} else if err != nil {
			log.Fatal(err)
		}
		// Step 3: displaying file contents
		fmt.Printf("Contents of %s:\n", hdr.Name)
		if _, err := io.Copy(os.Stdout, tr); err != nil {
			log.Fatal(err)
		}
		fmt.Println()
	}
}
