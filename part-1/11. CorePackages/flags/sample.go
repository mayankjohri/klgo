package main

import (
	"flag"
	"fmt"
)

func main() {
	var username string
	flag.StringVar(&username, "user", "rakesh", "user name")

	heightPtr := flag.Int("height", 68, "height")
	metricFlagPtr := flag.Bool("metric", true, "metric / imperial")

	flag.Parse()

	fmt.Println("username:", username)
	fmt.Println("height:", *heightPtr)
	fmt.Println("fork:", *metricFlagPtr)
	fmt.Println("Remaining:", flag.Args())
}
