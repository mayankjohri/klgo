package main

/*
This program will wait for 3 seconds before timeout
*/

import (
	"fmt"
	"time"
)

var c chan int

func handle(int) {}

func main() {
	select {
	case m := <-c:
		handle(m)
	case <-time.After(3 * time.Second):
		fmt.Println("Timeout happend after 3 seconds")
	}
}
