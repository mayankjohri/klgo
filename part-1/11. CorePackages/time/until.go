package main

import (
	"fmt"
	"time"
)

func main() {
	tStart := time.Now()
	time.Sleep(2 * time.Second)
	fmt.Println(time.Until(tStart))
}
