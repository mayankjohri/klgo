package main

import (
	"fmt"
	"time"
)

func sleep(seconds int, endGame chan<- bool) {
	time.Sleep(time.Duration(seconds) * time.Second)
	endGame <- true
}

func main() {
	endGame := make(chan bool, 1)
	go sleep(3, endGame)
	// go sleep(10, endGame)
	var end bool

	for !end {
		select {
		case end = <-endGame:
			fmt.Println("Thanks for waiting, the task is completed")
		case <-time.After(5 * time.Second):
			fmt.Println("Sorry failed the get the response in time thus exiting!")
			end = true
		}
	}

}
