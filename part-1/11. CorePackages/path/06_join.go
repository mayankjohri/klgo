package main

import (
	"fmt"
	"path"
)

func main() {
	fmt.Println(path.Join("etc", "x11", "video.txt"))
	fmt.Println(path.Join("usr", "bin/ls"))
	fmt.Println(path.Join("usr/bin", "ls"))

	fmt.Println(path.Join("/home/mayank/test", "../../goUser"))

	fmt.Println(path.Join("", ""))
	fmt.Println(path.Join("etc", ""))
	fmt.Println(path.Join("", "etc"))
}
