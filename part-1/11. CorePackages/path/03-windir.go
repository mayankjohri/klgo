package main

import (
	"fmt"
	"path/filepath"
)

func main() {
	fmt.Println(filepath.Dir("/etc/x11/test"))
	fmt.Println(filepath.Dir("etc/init.d/var.sh"))
	fmt.Println(filepath.Dir("/home/"))
	fmt.Println(filepath.Dir("/home"))
	fmt.Println(filepath.Dir("http://www.mj.com/testPath"))
	fmt.Println(filepath.Dir("/"))
	fmt.Println(filepath.Dir("c:\\test\testPath"))
}
