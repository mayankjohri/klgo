package main

import (
	"fmt"
	"path"
)

func main() {
	fmt.Println(path.Base("/etc/gdm"))
	fmt.Println(path.Base("/"))
	fmt.Println(path.Base("/home/mayank/go"))
	fmt.Println(path.Base("C://Windows/System32"))
	fmt.Println(path.Base("C:\\Windows\\System32"))
}
