package main

import (
	"errors"
	"fmt"
)

var _ error = (*CustomError)(nil) // ensure CustomError implements error

type CustomError struct {
	msg string
}

func (e CustomError) Error() string {
	return e.msg
}

func main() {
	// Methods return pointers to errors, allowing them to be nil
	err := &CustomError{
		"This is Custom Error!",
	}

	var eval *CustomError

	as := errors.As(err, &eval)      // yes, that's **CustomError
	asFaulty := errors.As(err, eval) // no compile error, so it wrongly seems okay
	is := errors.Is(err, eval)       // that's just *CustomError

	fmt.Printf(
		"as: %t, asFaulty: %t, is: %t\n",
		as,
		asFaulty,
		is,
	) // as: true, asFaulty: false, is: true
}
