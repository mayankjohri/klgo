package main

/*
Unwrap returns the result of calling the Unwrap method on err, if err's type
contains an Unwrap method returning error. Otherwise, Unwrap returns nil.
*/
import (
	"errors"
	"fmt"
)

func main() {
	// error 1 wrapped by 2 wrapped by 3
	err1 := errors.New("error 1")
	err2 := fmt.Errorf("error 2: %w", err1)
	err3 := fmt.Errorf("error 3: %w", err2)
	for currentErr := err3; errors.Unwrap(currentErr) != nil; {
		fmt.Println(currentErr)
		currentErr = errors.Unwrap(currentErr)
	}
}
