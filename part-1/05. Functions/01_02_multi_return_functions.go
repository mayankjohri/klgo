package main

import "fmt"

func custom_sum(x int16, y int16) (int16, int8) {

	switch {
	case x <= 32760:
		{
			fmt.Println("adding")
			return x + y, 0
		}
	case x > 32760:
		{
			return 0, -1
		}
	default:
		return 0, 0
	}
}

func main() {
	val, err := custom_sum(10, 11)
	fmt.Println(val, err)
	val, err = custom_sum(32761, 11)
	fmt.Println(val, err)
}
