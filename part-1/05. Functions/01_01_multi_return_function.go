package main

import "fmt"

func sumAndSub(x int, y int) (int, int) {
	return x + y, x - y
}

func main() {
	fmt.Println(sumAndSub(10, 11))
}
