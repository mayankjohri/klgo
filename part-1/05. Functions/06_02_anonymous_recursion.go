package main

import "fmt"

func main() {
	var seq func(x int) int

	seq = func(x int) int {
		var val int
		if x != 0 {
			val = x + seq(x-1)
		} else {
			val = 1
		}
		return val
	}

	for i := 1; i < 999999; i++ {
		fmt.Println(seq(i))
	}

}
