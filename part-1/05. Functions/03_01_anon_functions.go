package main

import "fmt"

type Point struct {
	x float64
	y float64
}

func vol_ndim_tube_v2(height float64, dims ...Point) float64 {
	var next Point
	area := 0.0
	for i, p := range dims {
		if i == len(dims)-1 {
			next = dims[0]
		} else {
			next = dims[i+1]
		}
		fmt.Println(next, p)
		area += next.x*p.y - next.y*p.x
		next = p
	}
	abs := func(n float64) float64 {
		if n < 0 {
			n = -n
		}
		return n
	}
	return abs(area/2) * height
}

func main() {
	nums := []Point{{2, 2}, {4, 10}, {9, 7}, {11, 2}}
	area := vol_ndim_tube_v2(10, nums...)
	fmt.Println(area)
}
