package main

import "fmt"

func sum(a int, b int, total *int) {
	*total = a + b
}

func main() {
	var x int
	sum(10, 20, &x)
	fmt.Println(x)
}
