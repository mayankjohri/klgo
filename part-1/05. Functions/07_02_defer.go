package main

import "fmt"

func addme(a int, b int) bool {
	var x int
	d := func() bool {
		fmt.Println(a, b, x)
		return (a + b) == x
	}
	defer d()
	x = a - b
	return (a - b) == x
}

func incre() int {
	x := 1
	d := func() int {
		x++
		return x
	}
	defer d()
	x--
	return x
}
func main() {
	fmt.Println(addme(10, 12))
	fmt.Println(incre())
}
