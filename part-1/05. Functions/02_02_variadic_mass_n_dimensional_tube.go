package main

import "fmt"

type Point struct {
	x float64
	y float64
}

func abs(n float64) float64 {
	if n < 0 {
		n = -n
	}
	return n
}

func vol_ndim_tube_v1(height float64, dims ...Point) float64 {
	// https://www.mathopenref.com/coordpolygonarea.html
	// we have n-dimentional tube with uneven sides and
	// with finite height and we have to find the volume
	// of that tube for that we will first find the area
	// of the side using various points and then multiple
	// the area with height to find the volumn using the
	// formula:
	// area = [(x1*y2 − y1*x2) + (x2*y3 − y2*x3) ... +(xn*y1 −yn*x1)]/2

	next := dims[0]
	area := 0.0

	for _, p := range dims[1:] {
		fmt.Println(next, p)
		area += next.x*p.y - next.y*p.x
		next = p
	}
	fmt.Println(dims[0], next)
	area += dims[0].y*next.x - dims[0].x*next.y
	return abs(area/2) * height
}

func vol_ndim_tube_v2(height float64, dims ...Point) float64 {

	var next Point
	area := 0.0

	for i, p := range dims {
		if i == len(dims)-1 {
			next = dims[0]
		} else {
			next = dims[i+1]
		}
		fmt.Println(next, p)
		area += next.x*p.y - next.y*p.x
		next = p
	}

	return abs(area/2) * height
}
func main() {

	nums := []Point{{2, 2}, {4, 10}, {9, 7}, {11, 2}}
	/* If you already have multiple args in a slice,
	   apply them to a variadic function using
	   func(slice...) like this. */

	area := vol_ndim_tube_v2(10, nums...)
	fmt.Println(area)
}
