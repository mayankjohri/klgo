package main

import "fmt"

func sum(a int, b int) int {
	return a + b
}

func multi(a int, b int) int {
	return a * b
}

func square(a int) int {
	return a * a
}

func action(a int, b int, f func(int, int) int) {
	fmt.Println(f(a, b))
}

func main() {
	action(10, 32, sum)
	action(10, 32, multi)
	// action(10, 32, square)
}
