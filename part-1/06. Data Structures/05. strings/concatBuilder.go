package main

import (
	"fmt"
	"strings"
)

func main() {
	var welcome strings.Builder
	welcome.Grow(32)
	fmt.Fprintln(&welcome, "Welcome to ", "India")
	fmt.Println(welcome.String()) // no copying
}
