package main

import (
	"bytes"
	"fmt"
	"html/template"
)

func main() {
	msgTmplt := "Welcome to {{.city}}, The City of {{.attr}}"
	data := map[string]interface{}{
		"city": "Bhopal",
		"attr": "Lakes",
	}
	t := template.Must(template.New("msg").Parse(msgTmplt))
	buf := &bytes.Buffer{}
	if err := t.Execute(buf, data); err != nil {
		panic(err)
	}
	msg := buf.String()
	fmt.Println(msg)
}
