package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := "ॐ भूर्भुवः स्वः तत्सवितुर्वरेण्यं भर्गो देवस्य धीमहि धियो यो नः प्रचोदयात् ॥"
	msg_eng := `oṃ bhūr bhuvaḥ svaḥ
    tat savitur vareṇyaṃ
    bhargo devasya dhīmahi
    dhiyo yo naḥ prachodayāt`
	fmt.Println(strings.Fields(msg))
	for _, word := range strings.Fields(msg) {
		fmt.Println(word)
	}
	fmt.Println(strings.Fields(msg_eng))
	for _, word := range strings.Fields(msg_eng) {
		fmt.Println(word)
	}
}
