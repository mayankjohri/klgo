package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := "Welcome"

	fmt.Println(strings.Repeat(msg, 4))
}
