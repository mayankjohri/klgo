
def factorial(n):
    lst = [n]
    if n > 1:
        lst.extend(factorial(n-1))
    return lst

print(factorial(5))
