package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := "Thanks for traveling in Lakeland"
	fmt.Println(strings.Count(msg, "a")) // `a` is present will return the count
	fmt.Println(strings.Count(msg, "A")) // `A` is not present it will return `0`
}
