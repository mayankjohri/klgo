package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := "Welcome to Budd Lake"

	indexer := func(r rune) bool {
		return string(r) == " "
	}

	fmt.Println(strings.Index(msg, "e"))
	fmt.Println(strings.IndexAny(msg, "dt"))
	fmt.Println(strings.IndexByte(msg, 66))  // 66 -> B
	fmt.Println(strings.IndexByte(msg, 121)) // 121 -> y
	fmt.Println(strings.IndexFunc(msg, indexer))
	fmt.Println(strings.IndexRune(msg, rune('B')))
}
