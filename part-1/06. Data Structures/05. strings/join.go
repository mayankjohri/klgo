package main

import (
	"fmt"
	"strings"
)

func main() {
	sur := []string{"Sa", "Re", "Ga", "Ma", "Pa", "Dha", "Ni", "sa"}
	fmt.Println(strings.Join(sur, ", "))
	name := []string{"Tejas"}
	fmt.Println(strings.Join(name, ", "))
	fmt.Println(strings.Join([]string{"a", "b"}, "ABC"))
}
