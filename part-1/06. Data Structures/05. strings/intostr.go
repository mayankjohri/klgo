package main

import (
	"fmt"
	"strconv"
)

func main() {
	val := 1231
	// int to str
	i := strconv.Itoa(val)
	fmt.Println(val, i)
	s := fmt.Sprint(val)
	fmt.Println(val, s)
}
