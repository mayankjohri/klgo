package main

import (
	"fmt"
	"strings"
)

func main() {
	var welcome strings.Builder
	// welcome.Grow(1)
	fmt.Fprint(&welcome, "Welcome to ", "India")
	welMessage := welcome.String()
	fmt.Println(welMessage)       // no copying
	fmt.Println(welcome.String()) // no copying
}
