package main

import (
	"fmt"
)

func main() {
	var sli []int
	sli = make([]int, 5, 10)
	fmt.Println("Its default initialized array", sli)
}
