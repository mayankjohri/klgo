package main

import (
	"fmt"
	"reflect"
)

func main() {

	nums := []int{
		36, 31, 64,
		45, 2, 1, 12, 6, 7, 38, 18,
		6, 7, 22, 21,
	}
	fmt.Println("Original array:", nums)
	slice_1 := nums[2:6]
	i := 2
	j := 10
	nums = append(nums[:i], append(make([]int, j), nums[i:]...)...)

	fmt.Println(reflect.TypeOf(nums))
	fmt.Println(reflect.TypeOf(slice_1))
	fmt.Println("slice_1", slice_1)
	fmt.Println("Original array:", nums)
}
