package main

import (
	"fmt"
	"strings"
)

func main() {
	SirPauly := "Mr. K.V. Pauly"
	mayank := "Mayank"

	fmt.Println(strings.HasSuffix(SirPauly, "ly"))
	fmt.Println(strings.HasSuffix(SirPauly, "Ly."))
	fmt.Println(strings.HasSuffix(mayank, "ly"))
}
