package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := "WelcOme tO The Budd Lake"

	fmt.Println(strings.Replace(msg, "O", "o", -1))
	fmt.Println(strings.Replace(msg, "O", "o", 1))
	fmt.Println(strings.ReplaceAll(msg, "O", "o"))
}
