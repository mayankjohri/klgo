/*
Slit the string at 4th character using FieldsFunc

*/
package main

import (
	"fmt"
	"strings"
	"unicode"
)

func main() {
	msg := `oṃ bhūr bhuvaḥ svaḥ
    tat savitur vareṇyaṃ
    bhargo devasya dhīmahi
    dhiyo yo naḥ prachodayāt`

	indx := 0
	splitter := func(c rune) bool {
		if indx < 4 {
			indx++
			return false
		}
		indx = 0
		return true
	}
	for _, word := range strings.FieldsFunc(msg, splitter) {
		if word != "" {
			fmt.Println(">", word, "<")
		}
	}

	splitter = func(c rune) bool {
		return !unicode.IsLetter(c)
	}
	fmt.Println("------------------")
	for _, word := range strings.FieldsFunc(msg, splitter) {
		if word != "" {
			fmt.Println(">", word, "<")
		}
	}

}
