package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := `Mr. Gandhi Ji`
	fmt.Println(">", strings.TrimPrefix(msg, "Mr."), "<")
	fmt.Println(">", strings.TrimSuffix(msg, "Ji"), "<")
}
