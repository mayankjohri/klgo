package main

import (
    "fmt"
    "math/rand"
    "time"
)

func main() {
    var (
        nums [10]int
    )

    rand.Seed(time.Now().UnixNano())
    /*
    for i := 0; i < len(nums); i++ {
        nums[i] = rand.Intn(111)
        fmt.Println("value:", nums[i], ", location:", i)
    }
    */
    for i, _ := range nums {
        nums[i] = rand.Intn(111)
        fmt.Println("value:", nums[i], ", location:", i)
    }
    
    fmt.Println("Lets read the values")
    for i, x := range nums {
        fmt.Println("value:", x, ", location:", i)
    }
}
