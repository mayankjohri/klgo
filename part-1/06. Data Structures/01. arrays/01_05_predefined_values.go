package main

import (
    "fmt"
)

func main() {

    nums := [10]int{
        36, 31, 64,
        45, 2, 1,
        6, 7, 38, 18,
    }

    fmt.Println("Lets read the values")
    for i, x := range nums {
        fmt.Println("value:", x, ", location:", i)
    }
}
