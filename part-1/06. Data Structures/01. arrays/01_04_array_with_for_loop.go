package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var (
		nums [10]int
	)

	rand.Seed(time.Now().UnixNano())
	for i, x := range nums {
		x = rand.Intn(111)
		fmt.Println("value:", x, ", location:", i)
	}

	fmt.Println("Lets read the values")
	for i, x := range nums {
		fmt.Println("value:", x, ", location:", i)
	}
}
