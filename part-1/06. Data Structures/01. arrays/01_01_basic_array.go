package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var nums [10]int

	// without below code `rand.Intn` will return same set of values
	// https://stackoverflow.com/questions/39529364/go-rand-intn-same-number-value
	rand.Seed(time.Now().UnixNano())

	for x := 0; x < 10; x++ {
		fmt.Println(x)
		nums[x] = rand.Intn(111)
	}
	fmt.Println(nums)
}
