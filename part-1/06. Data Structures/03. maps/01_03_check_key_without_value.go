package main

import (
	"fmt"
)

func checkKey(dict map[int]string, key int) bool {
	_, flg := dict[key]
	return flg
}

func main() {
	var pincode_locality = make(map[int]string)

	pincode_locality[462003] = "SOUTH T.T. NAGAR B.O."
	pincode_locality[465680] = "Naahli B.O"
	pincode_locality[465697] = "Biaora Mandu B.O"
	pincode_locality[466651] = "Muktarnagar B.O"
	fmt.Println(checkKey(pincode_locality, 465697))
	delete(pincode_locality, 465697)
	fmt.Println(checkKey(pincode_locality, 465697))
}
