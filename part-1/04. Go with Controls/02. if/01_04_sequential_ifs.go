package main

import "fmt"

func main() {
	x := 23
	if x > 25 {
		fmt.Println("Greater than 25")
	}
	if x > 23 {
		fmt.Println("Greater than 23")
	}
	if x > 20 {
		fmt.Println("Greater than 20")
	}
}
