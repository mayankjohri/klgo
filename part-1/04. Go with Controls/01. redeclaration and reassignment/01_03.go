package main

import "fmt"

func main() {
	a := 0
	b := "Maynak"
	{
		fmt.Println(a, b) // 0 Maynak
		a, b = 1, "Mayank Johri"
		fmt.Println(a, b) // 1 Mayank Johri
	}
	fmt.Println(a, b) // 1 Mayank Johri
}
