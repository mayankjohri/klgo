package main

import "fmt"

func main() {
    a := 101
    b := 201
    // new scope demarkated by "{" & "}"
    {
        a := "ओ३म्"       // new local var
        b--               // global variable updated
        fmt.Println(a, b) // ओ३म्  200
    }
    // original variables
    fmt.Println(a, b) // 101 200
}
