package main

import "fmt"

func main() {
	a := 100
	if a >= 100 {
		fmt.Println(a)
		// new local variable `a`
		a, b := 10, 10
		fmt.Println(a, b)
	}
	// this will error: `undefined: b`
	// fmt.Println(a, b)
	fmt.Println(a)
}
