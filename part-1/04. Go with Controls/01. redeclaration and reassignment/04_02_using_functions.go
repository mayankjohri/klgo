package main

import "fmt"

func increment(a int, b *int) {
    a++  // use as local var
    *b++ // use as global var
    fmt.Println(a, *b)
}

func main() {
    a := 101
    b := 201
    increment(a, &b)
    fmt.Println(a, b)
}
