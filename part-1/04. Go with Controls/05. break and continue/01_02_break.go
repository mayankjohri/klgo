package main

import "fmt"

func main() {
    for {
        for x := 0; x < 3; x++ {

            var provided string
            fmt.Print("\nPlease enter the password: ")
            fmt.Scanln(&provided)
            if provided == "xyz" {
                break
            }
            fmt.Println("Found some issue with password")
        }
        fmt.Println("3 invalid tries detected, please wait for 30 sec and try again...")
    }
}
