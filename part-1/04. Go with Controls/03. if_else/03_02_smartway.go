package main

import (
	"fmt"
)

func main() {
	var temp int
	fmt.Print("Please enter the current Temprature: ")
	fmt.Scanf("%d", &temp)

	if temp < 0 {
		fmt.Println("Freezing...")
	} else if temp <= 20 { // 0 <= temp <= 20
		fmt.Println("Cold")
	} else if temp <= 25 { // 21 <= temp <= 25
		fmt.Println("Room Temprature")
	} else if temp <= 35 { // 26 <= temp <= 35
		fmt.Println("Hot")
	} else {
		fmt.Println("Its very HOT!, lets stay at home... \nand drink lemonade.")
	}
}
