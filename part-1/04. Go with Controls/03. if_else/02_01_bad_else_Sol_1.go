package main

import "fmt"

func get_train_number(train_direction string) int {
	var train_number_down = 12156
	var train_number_up = 12155

	if train_direction == "up" {
		return train_number_up
	}
	return (train_number_down)

}

func main() {
	fmt.Println(get_train_number("up"))
	fmt.Println(get_train_number("down"))
}
