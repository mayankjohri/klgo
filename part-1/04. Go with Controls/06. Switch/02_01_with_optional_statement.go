package main

import "fmt"

func main() {

	switch x := 10; x {
	case 11:
		fmt.Println("greater than 10")
	case 9:
		fmt.Println("less than 10")
	case 10:
		fmt.Println("equal to 10")
	}
	// x will not be visible outside the switch code block
	//fmt.Println(x)
}
