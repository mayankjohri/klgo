package main

import (
    "fmt"
    "math"
)

func get_digit_numbers(i int) int {
    var x = 0
    for i >= 1 {
        i = i / 10
        x++
    }
    return x
}

func get_digit(i, n int) int {
    return i / int(math.Pow10(n-1))
}

func convert(i int) {
    switch i {
    case 0:
        fmt.Print("null")
    case 1:
        fmt.Print("eins")
    case 2:
        fmt.Print("zwei")
    case 3:
        fmt.Print("drei")
    case 4:
        fmt.Print("vier")
    case 5:
        fmt.Print("fünf")
    default:
        fmt.Println("Unknown Number")
    }
    fmt.Print(" ")
}

func main() {
    var i = 14250
    var cnt = get_digit_numbers(i)
    for i > 0 {
        x := get_digit(i, cnt)
        convert(x)
        cnt--
        i = i - int(x*int(math.Pow10(cnt)))

    }
    convert(i)
    fmt.Println("\nDone")
}
