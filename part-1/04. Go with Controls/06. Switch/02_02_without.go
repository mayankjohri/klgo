package main

import "fmt"

func main() {
	x := 10
	switch {
	case x > 10:
		fmt.Println("greater than 10")
	case x < 10:
		fmt.Println("less than 10")
	case x == 10:
		fmt.Println("equal to 10")
	}
	fmt.Println(x)
}
