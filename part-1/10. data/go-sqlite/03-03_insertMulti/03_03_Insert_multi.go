package main

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

const DB_FILE = "faqs_multi_import.db"

func check_err(msg string, err error) {
	if err != nil {
		panic(msg + ": " + err.Error())
	}
}

func check_count(rows *sql.Row) (count int) {
	err := rows.Scan(&count)
	check_err("check count", err)
	return count
}

type SubjectsRowStruct struct {
	subject string
}

func BulkInsertSubjects(db *sql.DB, sql string, unsavedRows []string) error {
	valueStrings := make([]string, 0, len(unsavedRows))
	valueArgs := make([]interface{}, 0, len(unsavedRows))
	for _, subject := range unsavedRows {
		valueStrings = append(valueStrings, "(?)")
		valueArgs = append(valueArgs, subject)
	}

	stmt := fmt.Sprintf(sql, strings.Join(valueStrings, ","))
	fmt.Println(stmt, valueArgs)
	_, err := db.Exec(stmt, valueArgs...)
	return err
}

func main() {
	db, err := sql.Open("sqlite3", DB_FILE)
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()
	db.Exec("PRAGMA journal_mode=WAL;")

	sqlStmt := `SELECT COUNT(*) FROM
                        sqlite_master WHERE type='table'
                        AND name='subjects';`
	rows := db.QueryRow(sqlStmt)

	if check_count(rows) == 0 {
		sqlStmt = `create table subjects
                        (id integer not null primary key,
                        name text NOT NULL);`
		_, err = db.Exec(sqlStmt)
		check_err("while creating table subjects", err)
	}

	subjects := []string{
		"Maths",
		"Physics",
		"Chemistry",
		"Computers",
	}

	sql_insert_subjects := "insert into subjects(name) values %s"
	err = BulkInsertSubjects(db, sql_insert_subjects, subjects)
	fmt.Println(err)
	check_err("BulkInsertSubjects", err)

	var count int

	err = db.QueryRow("select count(*) from subjects;").Scan(&count)
	if err != nil {
		fmt.Printf("QueryRow: %v\n", err)
	}
	fmt.Println(count)

}
