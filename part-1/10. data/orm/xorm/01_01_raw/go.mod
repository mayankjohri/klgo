module 01_01_raw

go 1.17

require (
	github.com/go-xorm/xorm v0.7.9
	github.com/mattn/go-sqlite3 v1.14.10
)

require (
	xorm.io/builder v0.3.6 // indirect
	xorm.io/core v0.7.2-0.20190928055935-90aeac8d08eb // indirect
)
