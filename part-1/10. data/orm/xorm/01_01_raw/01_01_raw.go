package main

import (
	"fmt"

	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-sqlite3"
)

const DB_FILE = "../db/faqs.db"

var engine *xorm.Engine

func check_err(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	engine, err := xorm.NewEngine("sqlite3", DB_FILE)
	check_err(err)
	results, err := engine.Query("select * from subjects")
	check_err(err)
	fmt.Println(results)
}
