module 04_01_iq

go 1.17

require (
	github.com/mattn/go-sqlite3 v1.14.10
	xorm.io/xorm v1.2.5
)

require (
	github.com/goccy/go-json v0.7.4 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	xorm.io/builder v0.3.9 // indirect
)
