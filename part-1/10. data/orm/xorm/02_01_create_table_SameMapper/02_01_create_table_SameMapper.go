package main

import (
	_ "github.com/mattn/go-sqlite3"
	"xorm.io/xorm"
)

const DB_FILE = "../db/faqs_xorm.db"

var engine *xorm.Engine

func check_err(err error) {
	if err != nil {
		panic(err)
	}
}

type InterviewSubjects struct {
	ID          int64  `xorm:"pk unique autoincr notnull"`
	SubjectName string `xorm:"varchar(30) unique notnull"`
}

func main() {
	orm, err := xorm.NewEngine("sqlite3", DB_FILE)
	check_err(err)
	defer orm.Close()
	// orm.ShowSQL(true)
	check_err(err)
	// lets create tables
	err = orm.CreateTables(&InterviewSubjects{})
	check_err(err)
}
