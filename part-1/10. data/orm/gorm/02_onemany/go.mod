module onemany/v1

go 1.17

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/kylelemons/godebug v1.1.0
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.0 // indirect
)
