package main

import "gorm.io/gorm"

type Students struct {
	gorm.Model
	Name  string
	Marks uint
}
