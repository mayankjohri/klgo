package main

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func main() {
	db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	// Migrate the schema
	db.AutoMigrate(&Students{})
	// Create
	db.Create(&Students{Name: "Rakesh Saxena", Marks: 999})

	// Query Read
	var Students Students
	// find Students with integer primary key
	db.First(&Students, 1)
	// find Students with Name Rakesh Sxanena
	db.First(&Students, "Name = ?", "Rakesh Sxanena")

	// Update - update Students's Marks to 200
	db.Model(&Students).Update("Marks", 1000)

	// Update - update multiple fields
	// db.Model(&Students).Updates(Students{Marks: 200, Name: "Ramesh"}) // non-zero fields
	db.Model(&Students).Updates(map[string]interface{}{"Marks": 200, "Name": "Ramesh"})

	// Delete - delete Students
	db.Delete(&Students, 1)
	db.Commit()
}
