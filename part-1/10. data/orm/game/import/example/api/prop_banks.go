package api

import (
	"net/http"

	"example.com/rest/example/dao"
	"example.com/rest/example/model"

	"github.com/gin-gonic/gin"
	"github.com/guregu/null"
	"github.com/julienschmidt/httprouter"
)

var (
	_ = null.Bool{}
)

func configPropBanksRouter(router *httprouter.Router) {
	router.GET("/propbanks", GetAllPropBanks)
	router.POST("/propbanks", AddPropBanks)
	router.GET("/propbanks/:argID", GetPropBanks)
	router.PUT("/propbanks/:argID", UpdatePropBanks)
	router.DELETE("/propbanks/:argID", DeletePropBanks)
}

func configGinPropBanksRouter(router gin.IRoutes) {
	router.GET("/propbanks", ConverHttprouterToGin(GetAllPropBanks))
	router.POST("/propbanks", ConverHttprouterToGin(AddPropBanks))
	router.GET("/propbanks/:argID", ConverHttprouterToGin(GetPropBanks))
	router.PUT("/propbanks/:argID", ConverHttprouterToGin(UpdatePropBanks))
	router.DELETE("/propbanks/:argID", ConverHttprouterToGin(DeletePropBanks))
}

// GetAllPropBanks is a function to get a slice of record(s) from prop_banks table in the main database
// @Summary Get list of PropBanks
// @Tags PropBanks
// @Description GetAllPropBanks is a handler to get a slice of record(s) from prop_banks table in the main database
// @Accept  json
// @Produce  json
// @Param   page     query    int     false        "page requested (defaults to 0)"
// @Param   pagesize query    int     false        "number of records in a page  (defaults to 20)"
// @Param   order    query    string  false        "db sort order column"
// @Success 200 {object} api.PagedResults{data=[]model.PropBanks}
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError
// @Router /propbanks [get]
// http "http://localhost:8080/propbanks?page=0&pagesize=20" X-Api-User:user123
func GetAllPropBanks(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)
	page, err := readInt(r, "page", 0)
	if err != nil || page < 0 {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	pagesize, err := readInt(r, "pagesize", 20)
	if err != nil || pagesize <= 0 {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	order := r.FormValue("order")

	if err := ValidateRequest(ctx, r, "prop_banks", model.RetrieveMany); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	records, totalRows, err := dao.GetAllPropBanks(ctx, page, pagesize, order)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	result := &PagedResults{Page: page, PageSize: pagesize, Data: records, TotalRecords: totalRows}
	writeJSON(ctx, w, result)
}

// GetPropBanks is a function to get a single record from the prop_banks table in the main database
// @Summary Get record from table PropBanks by  argID
// @Tags PropBanks
// @ID argID
// @Description GetPropBanks is a function to get a single record from the prop_banks table in the main database
// @Accept  json
// @Produce  json
// @Param  argID path int true "id"
// @Success 200 {object} model.PropBanks
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError "ErrNotFound, db record for id not found - returns NotFound HTTP 404 not found error"
// @Router /propbanks/{argID} [get]
// http "http://localhost:8080/propbanks/1" X-Api-User:user123
func GetPropBanks(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)

	argID, err := parseInt32(ps, "argID")
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	if err := ValidateRequest(ctx, r, "prop_banks", model.RetrieveOne); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	record, err := dao.GetPropBanks(ctx, argID)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeJSON(ctx, w, record)
}

// AddPropBanks add to add a single record to prop_banks table in the main database
// @Summary Add an record to prop_banks table
// @Description add to add a single record to prop_banks table in the main database
// @Tags PropBanks
// @Accept  json
// @Produce  json
// @Param PropBanks body model.PropBanks true "Add PropBanks"
// @Success 200 {object} model.PropBanks
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError
// @Router /propbanks [post]
// echo '{"id": 96,"created_at": "2074-12-07T22:17:56.884672095+05:30","updated_at": "2299-07-12T16:30:30.041913056+05:30","deleted_at": "2117-03-04T23:14:27.020206408+05:30","question_id": 28,"prop": "ropTKFPLwJfQrsqDRKumcBFEd"}' | http POST "http://localhost:8080/propbanks" X-Api-User:user123
func AddPropBanks(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)
	propbanks := &model.PropBanks{}

	if err := readJSON(r, propbanks); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := propbanks.BeforeSave(); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
	}

	propbanks.Prepare()

	if err := propbanks.Validate(model.Create); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := ValidateRequest(ctx, r, "prop_banks", model.Create); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	var err error
	propbanks, _, err = dao.AddPropBanks(ctx, propbanks)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeJSON(ctx, w, propbanks)
}

// UpdatePropBanks Update a single record from prop_banks table in the main database
// @Summary Update an record in table prop_banks
// @Description Update a single record from prop_banks table in the main database
// @Tags PropBanks
// @Accept  json
// @Produce  json
// @Param  argID path int true "id"
// @Param  PropBanks body model.PropBanks true "Update PropBanks record"
// @Success 200 {object} model.PropBanks
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError
// @Router /propbanks/{argID} [put]
// echo '{"id": 96,"created_at": "2074-12-07T22:17:56.884672095+05:30","updated_at": "2299-07-12T16:30:30.041913056+05:30","deleted_at": "2117-03-04T23:14:27.020206408+05:30","question_id": 28,"prop": "ropTKFPLwJfQrsqDRKumcBFEd"}' | http PUT "http://localhost:8080/propbanks/1"  X-Api-User:user123
func UpdatePropBanks(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)

	argID, err := parseInt32(ps, "argID")
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	propbanks := &model.PropBanks{}
	if err := readJSON(r, propbanks); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := propbanks.BeforeSave(); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
	}

	propbanks.Prepare()

	if err := propbanks.Validate(model.Update); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := ValidateRequest(ctx, r, "prop_banks", model.Update); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	propbanks, _, err = dao.UpdatePropBanks(ctx,
		argID,
		propbanks)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeJSON(ctx, w, propbanks)
}

// DeletePropBanks Delete a single record from prop_banks table in the main database
// @Summary Delete a record from prop_banks
// @Description Delete a single record from prop_banks table in the main database
// @Tags PropBanks
// @Accept  json
// @Produce  json
// @Param  argID path int true "id"
// @Success 204 {object} model.PropBanks
// @Failure 400 {object} api.HTTPError
// @Failure 500 {object} api.HTTPError
// @Router /propbanks/{argID} [delete]
// http DELETE "http://localhost:8080/propbanks/1" X-Api-User:user123
func DeletePropBanks(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)

	argID, err := parseInt32(ps, "argID")
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	if err := ValidateRequest(ctx, r, "prop_banks", model.Delete); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	rowsAffected, err := dao.DeletePropBanks(ctx, argID)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeRowsAffected(w, rowsAffected)
}
