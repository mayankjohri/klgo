package main

import (
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func main() {
	name := "testfile_temp.txt"
	fi, err := os.OpenFile(name, os.O_RDONLY|os.O_CREATE, 0764)
	check(err)

	err = fi.Close()
	check(err)
	new_file := "new_file.txt"
	err = os.Rename(name, new_file)
	check(err)

	fileInfo, err := os.Stat(new_file)
	check(err)

	fmt.Println("Is Directory: ", fileInfo.IsDir())
	fmt.Println("File Name:", fileInfo.Name())
	fmt.Println("File Size in bytes:", fileInfo.Size())
	fmt.Println("File Permissions:", fileInfo.Mode())
	fmt.Println("File Last modified:", fileInfo.ModTime())
}
