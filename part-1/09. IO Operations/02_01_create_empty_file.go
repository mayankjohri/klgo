package main

import (
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func main() {
	name := "testfile.txt"
	fi, err := os.OpenFile(name, os.O_RDONLY|os.O_CREATE, 0764)
	check(err)
	defer func() {
		err := fi.Close()
		check(err)
	}()
}
