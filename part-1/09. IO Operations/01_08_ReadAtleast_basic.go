package main

import (
	"fmt"
	"io"
	"strings"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Println(err)
	}
}

func main() {
	minlist := []int{4, 5}
	for _, x := range minlist {
		fmt.Println("Lets test with", x, "bytes as min.")
		r := strings.NewReader("Guten Morgan !!!")

		for {
			buf := make([]byte, 6)
			n, err := io.ReadAtLeast(r, buf, x)
			check(err)
			if n == 0 {
				break
			}
			fmt.Printf("%d: %s\n", n, buf)
		}
	}
}
