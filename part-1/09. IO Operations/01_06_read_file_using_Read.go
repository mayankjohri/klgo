package main

import (
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func main() {
	// open input file
	fi, err := os.Open("prayers.txt")
	check(err)

	// close `fi` on exit
	defer func() {
		err := fi.Close()
		check(err)
	}()

	// make a buffer to keep chunks that are read
	buf := make([]byte, 1024)
	for {
		// read a chunk and print it
		n, err := fi.Read(buf)
		check(err)
		if n == 0 {
			break
		}
		fmt.Printf("%s", buf)
	}
}
