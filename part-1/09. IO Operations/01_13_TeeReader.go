package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}
func EncryptPrintAndSave(r io.Reader, fileName string) {
	file, err := os.Create(fileName)
	defer file.Close()

	check(err)
	w := bufio.NewWriter(file)

	io.Copy(os.Stdout, io.TeeReader(r, w))
}

func main() {
	// resp, err := http.Get("http://www.golang.org")

	// if err != nil {
	//  log.Fatal("ERROR", err)
	// }
	// defer resp.Body.Close()

	fi, err := os.Open("prayers.txt")
	check(err)

	// close `fi` on exit
	defer func() {
		err := fi.Close()
		check(err)
	}()

	EncryptPrintAndSave(fi, "new_prayers.txt")
}
