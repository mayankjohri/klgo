package main

// url: https://gist.github.com/mohanson/6fa3b1d80ec0fce68b86008e945abaf2

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

var flAppend = flag.Bool("a", false,
	"append to the given FILEs, do not overwrite")

func main() {
	flag.Parse()

	var mode int
	mode = os.O_CREATE | os.O_WRONLY
	if *flAppend {
		mode = mode | os.O_APPEND
	} else {
		mode = mode | os.O_TRUNC
	}

	var writers []io.Writer
	for _, fname := range flag.Args() {
		fmt.Println(fname)
		file, err := os.OpenFile(fname, mode, 0666)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)
	r := io.TeeReader(os.Stdin, os.Stdout)
	if _, err := io.Copy(w, r); err != nil {
		log.Fatal(err)
	}
}
