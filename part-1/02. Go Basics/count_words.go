package main

import (
	"fmt"
	"regexp"
)

func getWordsFrom(text string) []string {
	words := regexp.MustCompile("\\w+")
	return words.FindAllString(text, -1)
}

func main() {
	words := `hello world. this is a good try.
            HAHAHA.TESTing`
	wc := getWordsFrom(words)
	fmt.Printf("%d\n", len(wc))
}
