package main

import "fmt"

func main() {
	
    var ( 

        ohm rune = 'ॐ'
	    chrOhm uint32 = 'ॐ'
	    iOhm uint32 = 2384
    )

	fmt.Println(ohm, chrOhm, iOhm)

	fmt.Printf("%c - %c - %c\n", ohm, chrOhm, iOhm)
}
