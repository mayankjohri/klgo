package main

import "github.com/dontpanic92/wxGo/wx"
import "time"

var g wx.Gauge

type MyFrame struct {
	wx.Frame
}

func (f *MyFrame) startUpload() {
	for {
		time.Sleep(time.Second)
		g.SetValue(g.GetValue() + 1)
		f.Refresh()
		f.Update()
	}
}

func NewMyFrame() MyFrame {
	f := MyFrame{}
	f.Frame = wx.NewFrame(wx.NullWindow, -1, "Test Thread")
	mainSizer := wx.NewBoxSizer(wx.HORIZONTAL)
	g = wx.NewGauge(f, wx.ID_ANY, 100, wx.DefaultPosition, wx.NewSize(600, 40), 0)
	f.SetSizer(mainSizer)
	mainSizer.Add(g, 100, wx.ALL|wx.EXPAND, 50)
	f.Layout()
	go f.startUpload()
	return f
}

func main() {
	wx1 := wx.NewApp()
	f := NewMyFrame()
	f.Show()
	wx1.MainLoop()
	return
}
