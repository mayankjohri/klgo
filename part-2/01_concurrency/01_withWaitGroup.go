package main

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"
)

// WaitGroup is used to wait for the program to finish goroutines.
var wg sync.WaitGroup

func PathWalker(root string) {
	// Schedule the call to WaitGroup's Done to tell goroutine is completed.
	defer wg.Done()
	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			fmt.Println(path)
		}
		return nil
	})
}

func main() {
	// runtime.GOMAXPROCS(1)
	wg.Add(2)
	go PathWalker("/var/log")
	go PathWalker("/etc")
	wg.Wait()
}
