package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"
)

func PathWalker(root string) {
	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			fmt.Println(path)
		}
		return nil
	})
}

func main() {
	go PathWalker("/var/log")
	go PathWalker("/etc")
	time.Sleep(5 * time.Second)
}
